
introduction
====================================================================

chorus is a simple Python module for chemical structure construction and drawing.


**Features**

- Requires Python 3.4 and upper
- Import molecule from V2000 Molfile or SMILES
- Draw molecule to SVG
- Draw molecule to PNG (requires PySide)
- Show molecular weight, formula, H-bond donor and acceptor
- Substructure search (VF2 algorithm in NetworkX)





* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
