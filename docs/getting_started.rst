
Getting started
====================================================================


**Installation**

*Mac*

If you don't have Python3,

ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
brew install python3

and then

pip3 install chorus







**Import Chemical structure**

*SMILES*


*CTAB file (.sdf, .mol)*



**Draw molecule to SVG**



**Calculate molecule properties**



* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
