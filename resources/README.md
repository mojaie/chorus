
Resources
------------

This directory contains chemical structure data obtained from external public chemical databases.

- DrugBank <https://www.drugbank.ca/>

DrugBank datasets are permitted to use under [Creative Common’s by-nc 4.0 License](https://creativecommons.org/licenses/by-nc/4.0/legalcode)
